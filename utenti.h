typedef struct UserListItem{
	struct UserListItem* next;
	char* username;
	struct sockaddr_in* client_addr;
}UserListItem;

typedef struct UserHead{
	int size;
	UserListItem* head;
}UserHead;

void List_init_node(char* username,struct sockaddr_in* client_addr,UserListItem* new_node);
void List_insert(UserHead* ul,UserListItem* nodo);
void List_init(UserHead* head);
int List_find_user(UserHead* ul,char* username);
char* List_delete(UserHead* list,char* name);
struct sockaddr_in* List_find_client(UserHead* ul,char* dst);
char* List_find_fromaddr(UserHead* ul,struct sockaddr_in* client_addr);
void UserListPrint(UserHead* ul);
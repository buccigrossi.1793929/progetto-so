#define TAKEN "Nome utente già in uso"
#define PASS_ERROR "Password sbagliata"
#define OK "Login effettuato \n"
#define SERVER_ADDR "127.0.0.1"
#define SERVER_CLOSE "Il server ha chiuso,terminare il programma con CTRL+Z\n"
#define USER_ERROR "\nNon puoi inviarti un messaggio da solo\n>>>"
#define OFFLINE_ERROR "\nQuesto utente è offline\n>>>"
#define QUIT "Quit"

typedef struct msg{
	char addressee[1024]; 
	char message[1024]; 
}msg;

int connectTCP(struct sockaddr_in *s_struct,int sockaddr_len);
int connectUDP(struct sockaddr_in *s_struct,int sockaddr_len);
void login(int sock_conn,int sock_udp1,struct sockaddr_in *s_struct);
void sendUDP(int sock_conn,struct sockaddr_in* s_struct);
void recvUDP(int sock_conn,struct sockaddr_in* s_struct);
void sendTCP(int sock_conn,char* msg);
void recvTCP(int sock_conn,char* msg);
int connectUDP(struct sockaddr_in *s_struct,int sockaddr_len);
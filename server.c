#include <sys/socket.h>
#include <errno.h>
#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>
#include <netinet/in.h>
#include <netinet/ip.h> 
#include <string.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h> 

#include <signal.h>

#include "utenti.h"
#include "server.h"


int sock_udp1;
int sock_udp2;
int sock_tcp;
thread_args* args;
pthread_t thread;
pthread_t thread_accept;

UserHead* ul;
char* list_users;
msg* s_msg;
msg* s_sendmsg;

char* quit=QUIT;

int error(char* msg){
	printf("%s \n",msg);
	exit(EXIT_FAILURE);
}

void sendTCP(int sock,char* msg){

	int msg_len=strlen(msg);
	int bytes_sent=0;
	int ret;

	while(bytes_sent<msg_len){
		ret = send(sock, msg + bytes_sent, msg_len - bytes_sent,0);
		if (ret == -1 && errno == EINTR) continue;
    	if (ret == -1) error("Errore nella send");
    	bytes_sent += ret;
	}

}

void recvTCP(int sock,char* msg){
	
	int ret;
	int bytes_recvs=0;
	do{
		ret=recv(sock,msg+bytes_recvs,1,0);
		if (ret == -1 && errno == EINTR) continue;
        	if (ret == -1) error("Errore nella read");
        	if (ret == 0) break;
		bytes_recvs++;
	}while(msg[bytes_recvs-1]!='\n');

	msg[bytes_recvs-1]='\0';

}

int connectTCP(struct sockaddr_in *s_struct,int sockaddr_len){

	int ret;

	int sock=socket(AF_INET,SOCK_STREAM,0);
	if(sock==-1) error("Errore nella socket");

	int reuseaddr_opt = 1;
    ret = setsockopt(sock,SOL_SOCKET, SO_REUSEADDR, &reuseaddr_opt, sizeof(reuseaddr_opt));
    if (ret==-1) error("Errore nella setsockopt");

	ret=bind(sock,(struct sockaddr*) s_struct,sockaddr_len);
	if(ret==-1) error("Errore nella bind");

	ret=listen(sock,10);
	if(ret==-1) error("Errore nella listen");

	return sock;
}


int connectUDP(struct sockaddr_in *s_struct,int sockaddr_len){

	int ret;

	int sock=socket(AF_INET,SOCK_DGRAM,0);
	if(sock==-1) error("Errore nella socket");

	int reuseaddr_opt = 1;
    ret = setsockopt(sock,SOL_SOCKET, SO_REUSEADDR, &reuseaddr_opt, sizeof(reuseaddr_opt));
    if (ret==-1) error("Errore nella setsockopt");

	ret=bind(sock,(struct sockaddr*)s_struct,sockaddr_len);
	if(ret==-1) error("Errore nella bind");

	return sock;

}

int checkpw(char* name,char* password){

	int ret;

	char pw[1024];
	char dir[1024]="users/";
	int plen=strlen(password);
	
	char* aux=malloc(1024);
	strcpy(aux,name);
	strcat(aux,".txt");
	strcat(dir,aux);

	int fd=open(dir,O_RDONLY | O_EXCL);
	if(fd==-1){
		free(aux);
		return -1;
	}

	ret=read(fd,pw,plen);
	if(ret==-1) error("Errore nella read");
	ret=close(fd);
	if(ret==-1) error("Errore nella close");

	free(aux);

	return strcmp(password,pw);
}


void initialize_user(char* user,char* password){
	
	char* u = malloc(1024);
	char dir[1024]="users/";
	strcpy(u,user);
	strcat(u,".txt");
	strcat(dir,u);


	int ret;
	int plen=strlen(password);

	int fd=open(dir,O_WRONLY | O_CREAT,0660);
	if(fd==-1) error("Errore nella open file");
	ret=write(fd,password,plen);
	if(ret==-1) error("Errore nella write file");
	ret=close(fd);
	if(ret==-1) error("Errore nella close file");

	free(u);
	return;
}

void online_list(int fd){
	int ret;
	UserListItem* item=ul->head;

	char* none="Nessun utente connesso al momento";

	while(item!=NULL){
		memset(list_users,0,sizeof(list_users));
		struct sockaddr_in* client=item->client_addr;
		int fromlen=sizeof(*client);

		if(ul->size==1){
			strcpy(list_users,none);
			strcpy(s_sendmsg->addressee,"list");
			strcpy(s_sendmsg->message,list_users);
			ret=sendto(fd,s_sendmsg,sizeof(msg),0,(struct sockaddr*)client,sizeof(*client));
			if(ret==-1) error("Errore nella sendto message");
			break;
		}

		char* name=item->username;
		UserListItem* aux=ul->head;

		while(aux!=NULL){
			if(strcmp(name,aux->username)!=0){
				strcat(list_users,aux->username);
				strcat(list_users,"-");
			}
			aux=aux->next;
		}
		strcpy(s_sendmsg->addressee,"list");
		strcpy(s_sendmsg->message,list_users);

		ret=sendto(fd,s_sendmsg,sizeof(msg),0,(struct sockaddr*)client,sizeof(*client));
		if(ret==-1) error("Errore nella sendto message");

		item=item->next;
	}
}

void leaving(char* name,int sock_udp2){
	char* bye=List_delete(ul,name);
	if(bye!=NULL){
		printf("(%s) ha abbandonato la chat \n",bye);
		online_list(sock_udp2);
	}
}

void messaging(int sock_udp1,int sock_udp2){

	int ret;

	char* uerr=USER_ERROR;
	char* offline_err=OFFLINE_ERROR;

	int uerrlen=strlen(uerr);
	int offlinelen=strlen(offline_err);
	int qlen=strlen(quit);

	char client_msg[1024];
	memset(client_msg,0,1024);
	
	char name_src[1024];
	memset(name_src,0,1024);

	struct sockaddr_in client={0};
	socklen_t len_struct=sizeof(struct sockaddr_in);

	int recv_bytes=0;
	ret=recvfrom(sock_udp1,s_msg,2048,0,(struct sockaddr*)&client,&len_struct);
	if (ret == -1) error("Errore sulla recvfrom");

	strcpy(client_msg,s_msg->message);
	strcpy(name_src,s_msg->addressee);

	if(memcmp(client_msg,QUIT,qlen)==0){
		leaving(name_src,sock_udp2);
		return;
	}

	int name_len=strlen(name_src);

	char* dst=strtok(client_msg,"-");
	char* txt=strtok(NULL,"");
	char* broad="All";

	if(strcmp(dst,broad)==0){

		printf("Invio messaggio da (%s) a (%s) ... \n",name_src,dst);

		UserListItem* aux=ul->head;

		while(aux){
			if(strcmp(aux->username,name_src)!=0){

				int fromlen=sizeof(*(aux->client_addr));

				strcpy(s_sendmsg->addressee,name_src);
				strcpy(s_sendmsg->message,txt);

				ret = sendto(sock_udp2,s_sendmsg,sizeof(msg),0,(struct sockaddr *)aux->client_addr,fromlen);
		    	if (ret == -1) printf("Errore nella sendto");

			}
			aux=aux->next;
		}

		printf("Inviato! \n");
		return;
	}

	struct sockaddr_in* client_dst=List_find_client(ul,dst);

	if(client_dst!=NULL){

		int fromlen=sizeof(*client_dst);

		if(strcmp(name_src,List_find_fromaddr(ul,client_dst))==0){
			strcpy(s_sendmsg->addressee,uerr);
			ret = sendto(sock_udp2,s_sendmsg,sizeof(msg),0,(struct sockaddr *)client_dst,fromlen);
    		if (ret == -1) error("Errore nella sendto");
    		return;
		}

		printf("Invio messaggio da (%s) a (%s) ... \n",name_src,dst);

		strcpy(s_sendmsg->addressee,name_src);
		strcpy(s_sendmsg->message,txt);
		ret = sendto(sock_udp2,s_sendmsg,sizeof(msg),0,(struct sockaddr *)client_dst,fromlen);
    	if (ret == -1) error("Errore nella sendto");

    	printf("Inviato!\n");

    }else{

    	client_dst=List_find_client(ul,name_src);

    	int fromlen=sizeof(*client_dst);
    	strcpy(s_sendmsg->addressee,offline_err);
    	
    	ret = sendto(sock_udp2,s_sendmsg,sizeof(msg),0,(struct sockaddr *)client_dst,fromlen);
    	if (ret == -1) error("Errore nella sendto");

    	printf("Inviato!\n");

    	return;
    }

}


void* handler(void* args){

	printf("Server pronto ad accettare nuove connessioni ...  \n");

	thread_args* arg=(thread_args*)args;

	int sock1=arg->fd1;

	int sock2=arg->fd2;


	while(1){
		messaging(sock1,sock2);
	}

	pthread_exit(NULL);
}


void signals(int signal){

	pthread_cancel(thread);
	pthread_join(thread,NULL);

	UserListItem* aux=ul->head;
	while(aux){

		int ret;

		strcpy(s_sendmsg->addressee,SERVER_CLOSE);
		strcpy(s_sendmsg->message,"");

        ret = sendto(sock_udp2,s_sendmsg,sizeof(msg),0,(struct sockaddr *)aux->client_addr,sizeof(*(aux->client_addr)));
    	if (ret == -1) error("Errore nella sendto");

    	free(aux->username);
    	free(aux->client_addr);
		
		aux=aux->next;
	}

	aux=ul->head;
	while(aux){
		UserListItem* item=aux;
		aux=aux->next;
		free(item);
	}

	free(s_sendmsg);
	free(s_msg);
	free(args);
	free(list_users);
	free(ul);

	int ret=close(sock_udp1);
	if(ret==-1) error("Errore nella close sock_udp1");

	ret=close(sock_udp2);
	if(ret==-1) error("Errore nella close sock_udp2");

	exit(EXIT_SUCCESS);
}


void getstarted(int sock_tcp,int sock_udp2){

	int ret;

	struct sockaddr_in client={0};

	char username[1024];
	char password[1024];
	char msg_udp[1024];

	memset(username, 0,sizeof(username));
	memset(password, 0,sizeof(password));
	memset(msg_udp,0,sizeof(msg_udp));

	socklen_t len_struct=sizeof(struct sockaddr_in);

	recvTCP(sock_tcp,username);
	recvTCP(sock_tcp,password);

	if(strlen(username)==0 || strlen(password)==0) return;

	//printf("Il client ha username %s \n",username);
	//printf("Il client ha password %s \n",password);

	if(List_find_user(ul,username)){
	  sendTCP(sock_tcp,TAKEN);
	}else{

		int test=checkpw(username,password);
		printf("Test password %d\n",test);

	 	if(test==-1){

	 	initialize_user(username,password);
	 	printf("Utente inizializzato\n");

        sendTCP(sock_tcp,OK);

    	ret=recvfrom(sock_udp2,msg_udp,1024,0,(struct sockaddr*)&client,&len_struct);
		if (ret == -1) error("Errore nella recv");

		UserListItem* new=malloc(sizeof(UserListItem));
	    if(new==NULL) error("malloc");
		new->username=malloc(1024);
		if(new->username==NULL) error("malloc");
		new->client_addr=malloc(sizeof(struct sockaddr_in));
		if(new->client_addr==NULL)error("malloc");

	
	    List_init_node(username,&client,new);
    	List_insert(ul,new);
		online_list(sock_udp2);
	    }
	    else if(test==0){

		sendTCP(sock_tcp,OK);

    	ret=recvfrom(sock_udp2,msg_udp,1024,0,(struct sockaddr*)&client,&len_struct);
		if (ret == -1) error("Errore nella recv");

		UserListItem* new=malloc(sizeof(UserListItem));
	    if(new==NULL) error("malloc");
		new->username=malloc(1024);
		if(new->username==NULL) error("malloc");
		new->client_addr=malloc(sizeof(struct sockaddr_in));
		if(new->client_addr==NULL)error("malloc");
	    
	    List_init_node(username,&client,new);
    	List_insert(ul,new);
		online_list(sock_udp2);

	}
	else{
		sendTCP(sock_tcp,PASS_ERROR);
	}

  }	

}


void* handler_logic(void* args){
	thread_acc_arg* arg=(thread_acc_arg*)args;

	int sock_conn=arg->sock_tcp;
	int sock_udp2=arg->sock_udp2;

    getstarted(sock_conn,sock_udp2);
    UserListPrint(ul);

	int ret=close(sock_conn);
	if(ret==-1) error("Errore nella close");
	free(args);

	pthread_exit(NULL);
}


int main(int argc, char** argv){

	int ret;

	s_msg=malloc(sizeof(msg));
	if(s_msg==NULL) error("malloc");
	s_sendmsg=malloc(sizeof(msg));
	if(s_sendmsg==NULL) error("malloc");
	list_users=malloc(1024);
	if(list_users==NULL) error("malloc");

	memset(s_sendmsg,0,sizeof(msg));

    struct sigaction action={0};
    action.sa_handler=signals;	

	ret=sigaction(SIGINT,&action,NULL);
	if(ret==-1) error("Errore nella sigaction");

	ret=sigaction(SIGQUIT,&action,NULL);
	if(ret==-1) error("Errore nella sigaction");

	ret=sigaction(SIGTERM,&action,NULL);
	if(ret==-1) error("Errore nella sigaction");

	ret=sigaction(SIGTSTP,&action,NULL);
	if(ret==-1) error("Errore nella sigaction");

	ul=malloc(sizeof(UserHead));
	if(ul==NULL) error("malloc");

	ul->head=NULL;
	ul->size=0;

	struct sockaddr_in s_struct_tcp = {0};
	struct sockaddr_in s_struct_udp1 = {0};
	struct sockaddr_in s_struct_udp2 = {0};

	int sockaddr_len = sizeof(struct sockaddr_in);
	
	s_struct_tcp.sin_family=AF_INET;
	s_struct_tcp.sin_addr.s_addr=INADDR_ANY;
	s_struct_tcp.sin_port=htons(4000);

	s_struct_udp1.sin_family=AF_INET;
	s_struct_udp1.sin_addr.s_addr=INADDR_ANY;
	s_struct_udp1.sin_port=htons(4001);

	s_struct_udp2.sin_family=AF_INET;
	s_struct_udp2.sin_addr.s_addr=INADDR_ANY;
	s_struct_udp2.sin_port=htons(4002);


	int sock_tcp=connectTCP(&s_struct_tcp,sockaddr_len);

	struct sockaddr_in client_struct = {0};

	sock_udp1=connectUDP(&s_struct_udp1,sockaddr_len);
    sock_udp2=connectUDP(&s_struct_udp2,sockaddr_len);


	args=malloc(sizeof(thread_args));

	if(args==NULL) error("Errore sugli args");
    args->fd1=sock_udp1;
    args->fd2=sock_udp2;


	ret=pthread_create(&thread,NULL,handler,(void*)args);
	if(ret==-1) error("Errore nella pthread_create");


	while(1){

		memset(&client_struct, 0, sizeof(struct sockaddr_in));

		int sock_acc=accept(sock_tcp,(struct sockaddr*)&client_struct,(socklen_t *)&sockaddr_len);
		if(sock_acc==-1) error("Errore nella accept");

		printf("[Connessione stabilita]\n");
        
        thread_acc_arg* accept_struct=malloc(sizeof(thread_acc_arg));
		if(accept_struct==NULL) error("Errore nella malloc");

		accept_struct->sock_tcp=sock_acc;
		accept_struct->sock_udp2=sock_udp2;

		ret=pthread_create(&thread_accept,NULL,handler_logic,(void*)accept_struct);
		if(ret==-1) error("Errore nella pthread_create");

		ret=pthread_detach(thread_accept);
		if(ret!=0) error("Errore nella pthread_detach");

	}

	return EXIT_SUCCESS;
}

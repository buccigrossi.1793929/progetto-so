#include "utenti.h"
#include <assert.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <arpa/inet.h>


void List_init(UserHead* ul){
	ul=malloc(sizeof(UserHead));
	ul->head=NULL;
	ul->size=0;
}

char* List_delete(UserHead* ul,char* name){
	UserListItem* aux=ul->head;
	UserListItem* prev;
	if(strcmp(aux->username,name)==0){
		ul->head=aux->next;
		free(aux->username);
		free(aux->client_addr);
		free(aux);
		ul->size--;
		return name;
	}

	while(aux!=NULL && strcmp(aux->username,name)!=0){
		prev=aux;
		aux=aux->next;
	}

	if(aux==NULL) return NULL;
	prev->next=aux->next;

	free(aux->username);
	free(aux->client_addr);
	free(aux);

	ul->size--;

	return name;
}

void List_init_node(char* username,struct sockaddr_in* client_addr,UserListItem* node){

	strcpy(node->username,username);
	memcpy(node->client_addr,client_addr,sizeof(struct sockaddr_in));
	node->next=NULL;

	return;
}

void List_insert(UserHead* ul,UserListItem* node){

	if(ul->head==NULL){
		ul->head=node;
		ul->size++;
		return;
	}

	UserListItem* tmp=ul->head;
	while(tmp->next!=NULL)
		tmp=tmp->next;
	tmp->next=node;
	ul->size++;
}

void UserListPrint(UserHead* ul){
  char client_ip[INET_ADDRSTRLEN];	 
  UserListItem* aux=ul->head;
  printf("Numero di utenti connessi: %d\n",ul->size);
  while(aux){
    inet_ntop(AF_INET, &(aux->client_addr->sin_addr), client_ip, INET_ADDRSTRLEN);
    printf("Pronto a chattare :[%s]-IP[%s]-PORTA[%hd]\n", aux->username,client_ip,aux->client_addr->sin_port);
    aux=aux->next;
  }
}

int List_find_user(UserHead* ul,char* username) {
  UserListItem* tmp= ul->head;
  while(tmp){
    if (strcmp(tmp->username,username)==0)
      return 1;
    tmp=tmp->next;
  }
  return 0;
}

struct sockaddr_in* List_find_client(UserHead* ul,char* dst){
	UserListItem* aux=ul->head;
	while(aux){
		if (strcmp(aux->username,dst)==0)
  			return aux->client_addr;
	aux=aux->next;
	}
	return 0;
}
char* List_find_fromaddr(UserHead* ul,struct sockaddr_in* client_addr){
	UserListItem* aux=ul->head;
	while(aux){
		if (aux->client_addr->sin_port==client_addr->sin_port)
  			return aux->username;
	aux=aux->next;
	}
	return 0;
}

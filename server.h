#define TAKEN "Nome utente già in uso\n"
#define PASS_ERROR "Password sbagliata\n"
#define OK "Login effettuato \n"
#define SERVER_ADDR "127.0.0.1"
#define SERVER_CLOSE "Il server ha chiuso,terminare il programma con CTRL+Z\n"
#define USER_ERROR "\nNon puoi inviarti un messaggio da solo\n>>>"
#define OFFLINE_ERROR "\nQuesto utente è offline\n>>>"
#define QUIT "Quit"



typedef struct thread_args{
	int fd1;
	int fd2;
}thread_args;

typedef struct msg{
	char addressee[1024];
	char message[1024];
}msg;

typedef struct thread_acc_arg{
	int sock_tcp;
	int sock_udp2;
}thread_acc_arg;

int connectTCP(struct sockaddr_in *server_struct,int sockaddr_len);
int connectUDP(struct sockaddr_in *server_struct,int sockaddr_len);
void recvTCP(int fd_conn,char* msg);
void sendTCP(int fd_conn,char* msg);
#include <sys/socket.h>
#include <errno.h>
#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <string.h>
#include <arpa/inet.h> 
#include <fcntl.h> // for open
#include <unistd.h> // for close
#include <time.h>

#include <signal.h>

#include "client.h"

int sock_udp1;
int sock_udp2;
struct sockaddr_in s_struct_udp1;
struct sockaddr_in s_struct_udp2;
pthread_t thread;
char server_ip[16];

msg* s_msg;
msg* s_recvmsg;

int pass;
int verify;
char* quit=QUIT;

time_t t;
struct tm *now ;

void error(char* msg){
	printf("%s\n",msg);
	exit(EXIT_FAILURE);
}


void sendTCP(int sock,char* msg){

	int msg_len=strlen(msg);
	int bytes_sent=0;
	int ret;
	while(bytes_sent<msg_len){
		ret = send(sock, msg + bytes_sent, msg_len - bytes_sent,0);
		if (ret == -1 && errno == EINTR) continue;
    	if (ret == -1) error("Errore nella send");
    	bytes_sent += ret;
	}

}
void recvTCP(int sock_conn,char* msg){
	
	int ret;
	int recv_bytes=0;
	do{
		ret=recv(sock_conn,msg+recv_bytes,1,0);
		if (ret == -1 && errno == EINTR) continue;
        	if (ret == -1) error("Errore nella recv");
        	if (ret == 0) break;
		recv_bytes++;
	}while(msg[recv_bytes-1]!='\n');

	msg[recv_bytes-1]='\0';

}

void recvUDP(int sock_conn,struct sockaddr_in* s_struct){
	
	int ret;
	char* online;
	socklen_t len_struct=sizeof(struct sockaddr_in);

	ret=recvfrom(sock_conn,s_recvmsg,sizeof(msg),0,NULL,NULL);
	if (ret == -1) error("Errore nella recvfrom");

	if(strcmp(s_recvmsg->addressee,"list")==0){
		printf("Utenti disponibili al momento : [");
		online=strtok(s_recvmsg->message,"-");
		while(online){
			printf("(%s)",online);
			online=strtok(NULL,"-");
		}
		printf("]\n\n");
	}else if(strcmp(s_recvmsg->addressee,SERVER_CLOSE)==0){
		printf("%s\n",s_recvmsg->addressee);
		verify=0;	
		pthread_exit(NULL);
	}
	
	else if(strcmp(s_recvmsg->addressee,USER_ERROR)==0){
		printf("%s\n",s_recvmsg->addressee);
	}else if(strcmp(s_recvmsg->addressee,OFFLINE_ERROR)==0){
		printf("%s\n",s_recvmsg->addressee);
    }
    else{
    	time(&t);
    	now=localtime(&t);
		printf("\n%02d:%02d\n[%s]:%s\n>>>\n",now->tm_hour, now->tm_min,s_recvmsg->addressee,s_recvmsg->message);
	}

}



void login(int sock_conn,int sock_udp2,struct sockaddr_in *s_struct){

	char password[1024];
	char username[1024];
	char result[1024];
	int ret;

	memset(password,0,sizeof(password));
	memset(username,0,sizeof(username));
	memset(result,0,sizeof(result));


    printf("Inserisci nome utente: ");
    if(fgets(username, sizeof(username), stdin)!=(char*)username){
    	error("Non riesco ad ottenere nome utente");    
    }
    int user =strlen(username);
    username[user-1]='\n';

    sendTCP(sock_conn,username);

    printf("Inserisci password: ");
    if(fgets(password, sizeof(password), stdin)!=(char*)password){
    	error("Non riesco ad ottenere password");
	}
	int pass=strlen(password);
	password[pass - 1] = '\n';

	sendTCP(sock_conn,password);

	int fromlen=sizeof(*s_struct);

	recvTCP(sock_conn,result);
	printf("%s\n",result);

	if((strcmp(result,TAKEN)!=0 && strcmp(result,PASS_ERROR)!=0)){
		username[user - 1] = '\0';
		strcpy(s_msg->addressee,username);
		ret = sendto(sock_udp2," ",1024,0,(struct sockaddr *)s_struct,fromlen);
		if(ret == -1) error(strerror(errno));
		return;
	}

	else{
		
		free(s_msg);
		free(s_recvmsg);
		exit(EXIT_FAILURE);
	}

}


void* c_handler(void* args){

	while(1){
		recvUDP(sock_udp2,&s_struct_udp2);
	}
	pthread_exit(NULL);
}


int connectTCP(struct sockaddr_in *s_struct,int sockaddr_len){

    int ret;

    int sock=socket(AF_INET, SOCK_STREAM, 0);
    if(sock==-1) error("Errore nella socket");

    ret = connect(sock, (struct sockaddr*) s_struct, sockaddr_len);
    if(ret==-1) error("Errore nella connect"); 

    return sock;
}

int connectUDP1(struct sockaddr_in *s_struct,int sockaddr_len){

	s_struct->sin_family =AF_INET;                    
    s_struct->sin_addr.s_addr = inet_addr(server_ip);    
    s_struct->sin_port = htons(4001);


	int fd=socket(AF_INET,SOCK_DGRAM,0);
	if(fd==-1) error("Errore nella socket udp1");

	return fd;

}

int connectUDP2(struct sockaddr_in *server_struct,int sockaddr_len){

	server_struct->sin_family =AF_INET;                    
    server_struct->sin_addr.s_addr = inet_addr(server_ip);    
    server_struct->sin_port = htons(4002);

	int fd=socket(AF_INET,SOCK_DGRAM,0);
	if(fd==-1) error("Errore nella socket udp2");

	return fd;

}


void sendUDP(int sock_conn,struct sockaddr_in* s_struct){
	
	int ret;
	int qlen=strlen(quit);
	int qmsg=1;

	char* aux;
	char mess[1024];
	memset(mess,0,sizeof(mess));

	if(fgets(mess, sizeof(mess), stdin)!=(char*)mess) error("Non sono riuscito a leggere l'input");

	int mess_len=strlen(mess);
	mess[mess_len - 1] = '\0';

	qmsg=memcmp(mess,quit,qlen);

	aux=strchr(mess,'-');
	if(aux!=NULL && strlen(aux)>1 &&verify!=0){

		strcpy(s_msg->message,mess);

		int fromlen=sizeof(*s_struct);

		ret = sendto(sock_conn,s_msg,2048,0,(struct sockaddr *)s_struct,fromlen);
		if (ret == -1) error("Errore nella sendto");
	}

	else if(qmsg==0){

		if(verify!=0){

			strcpy(s_msg->message,mess);
			ret = sendto(sock_conn,s_msg,2048,0,(struct sockaddr *)s_struct,sizeof(*s_struct));
			if (ret == -1) error(strerror(errno));

		}

		pthread_cancel(thread);
		pthread_join(thread,NULL);

		ret=close(sock_udp1);
		if(ret==-1) error("Errore nella close");
		ret=close(sock_udp2);
		if(ret==-1) error("Errore nella close");

		free(s_msg);
		free(s_recvmsg);

		exit(EXIT_SUCCESS);

	}else if(verify!=0){
		printf("Invia un messaggio nella forma utente-messaggio \n");
	}else{
		printf("Siamo spiacienti ma il server è chiuso , arrestare il programma\n");
	}

}

void signals(int signal){

	int ret;

	pthread_cancel(thread);
	pthread_join(thread,NULL);

	if(verify!=0){

		strcpy(s_msg->message,QUIT);

		ret = sendto(sock_udp1,s_msg,2048,0,(struct sockaddr *)&s_struct_udp1,sizeof(s_struct_udp1));
		if (ret == -1) error(strerror(errno));
	}


	ret=close(sock_udp1);
	if(ret==-1) error("Errore nella close");
	ret=close(sock_udp2);
	if(ret==-1) error("Errore nella close");

	free(s_msg);
	free(s_recvmsg);

	exit(EXIT_SUCCESS);
}



int main(int argc, char** argv){

    printf("Benvenuto in questa chat privata!\nPotrai scambiare nessaggi con altri utenti nel formato utente-messaggio\n");
    printf("Prima però abbiamo bisogno di identificarti ... \n\n");

	int ret;
	verify=0;

	s_msg=malloc(sizeof(msg));
	if(s_msg==NULL) error("malloc");
	s_recvmsg=malloc(sizeof(msg));
	if(s_recvmsg==NULL) error("malloc");
	memset(s_recvmsg,0,sizeof(msg));
	memset(s_msg,0,sizeof(msg));

    struct sigaction action={0};
	action.sa_handler=signals;

	ret=sigaction(SIGINT,&action,NULL);
	if(ret==-1) error("Errore nella sigaction");

	ret=sigaction(SIGQUIT,&action,NULL);
	if(ret==-1) error("Errore nella sigaction");
	
	ret=sigaction(SIGTERM,&action,NULL);
	if(ret==-1) error("Errore nella sigaction");
	
	ret=sigaction(SIGTSTP,&action,NULL);
	if(ret==-1) error("Errore nella sigaction");

	strcpy(server_ip,SERVER_ADDR);
	if(argc==2){
		strcpy(server_ip,argv[1]);
	}

    struct sockaddr_in s_struct_tcp = {0};
    memset(&s_struct_tcp,0,sizeof(struct sockaddr_in));        //
    memset(&s_struct_udp1,0,sizeof(struct sockaddr_in));

    int sockaddr_len = sizeof(struct sockaddr_in);

    s_struct_tcp.sin_addr.s_addr = inet_addr(server_ip);
    s_struct_tcp.sin_family = AF_INET;
    s_struct_tcp.sin_port = htons(4000); 

    int sock_tcp=connectTCP(&s_struct_tcp,sockaddr_len);

    sock_udp1=connectUDP1(&s_struct_udp1,sockaddr_len);
    sock_udp2=connectUDP2(&s_struct_udp2,sockaddr_len);

    login(sock_tcp,sock_udp2,&s_struct_udp2);

    verify=1;

    ret=close(sock_tcp);
    if(ret==-1) error("Errore sulla close");

    ret=pthread_create(&thread,NULL,c_handler,NULL);
    if(ret==-1) error("Errore sulla create");

    while(1){
    	sendUDP(sock_udp1,&s_struct_udp1);
       }

	return 0;


}

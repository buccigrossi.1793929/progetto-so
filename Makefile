CC=gcc
CCOPTS=-pthread
CCOPTS2=-lasound

OBJS=server.o\
utenti.o\
client.o\

HEADERS=utenti.h\
server.h\
client.h


BINS=server\
client\

.phony: clean all

all: $(BINS)

%.o:	%.c $(HEADERS)
	$(CC) $(CCOPTS) -c -o $@  $<

server:	server.c utenti.c
	$(CC) $(CCOPTS) -g -o $@ $^ 

client:	client.c
	$(CC) $(CCOPTS) -g -o $@ $^ 

clean:
	rm -rf *.o *~ $(BINS)
	rm -rf users/*.txt
